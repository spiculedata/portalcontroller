from adafruit_bitmap_font import bitmap_font
from adafruit_display_text.label import Label
from micropython import const

SCREEN_WIDTH = const(320)
SCREEN_HEIGHT = const(240)
#WHITE = 0xffffff
#RED = 0xff0000
#YELLOW = 0xffff00
#GREEN = 0x00ff00
#BLUE = 0x0000ff
#PURPLE = 0xff00ff
#BLACK = 0x000000
FONT = bitmap_font.load_font("/fonts/Helvetica-Bold-16.bdf")
FONT.load_glyphs(b'abcdefghjiklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ()')
TEXT_HEIGHT = Label(FONT, text="M", color=0x03AD31, max_glyphs=10)
TAPS_HEIGHT = const(40)
