import gc
gc.collect()
from adafruit_button import Button
gc.collect()
gc.collect()
from lightbuttons import LightButtons
gc.collect()
from display import Displays
gc.collect()
import consts
gc.collect()
from musicbuttons import MusicButtons
gc.collect()
from micropython import const
gc.collect()
class UI:
    TABS_X = const(5)
    TABS_Y = const(50)
    BUTTON_HEIGHT = const(40)
    BUTTON_WIDTH = const(80)

    lightbuttons = None
    musicbuttons = None

    # We want three buttons across the top of the screen

    TAPS_WIDTH = int(consts.SCREEN_WIDTH / 3)
    TAPS_Y = const(0)

    # We want two big buttons at the bottom of the screen
    BIG_BUTTON_HEIGHT = int(consts.SCREEN_HEIGHT / 3.2)
    BIG_BUTTON_WIDTH = int(consts.SCREEN_WIDTH / 2)
    BIG_BUTTON_Y = int(consts.SCREEN_HEIGHT - BIG_BUTTON_HEIGHT)
    views = {}
    buttons = []
    def __init__(self, publisher, config):
        self.publisher = publisher
        publisher.evt_foo += self.handle_foo

        self.displays = Displays(3)

        self.createTabs()

        self.views['button_view1'].selected = True
        self.views['button_view2'].selected = True
        self.displays.showLayer('view1')
        self.displays.hideLayer('view2')

        self.displays.showDisplay('splash')

        self.lightbuttons = LightButtons(self.displays, 'view1', publisher)
        self.musicbuttons = MusicButtons(self.displays, 'view2', publisher, config)
        self.lightbuttons.setVisible(True)

    def createTabs(self):
        # Main User Interface Buttons
        self.views['button_view1'] = Button(x=0, y=0, name="lights",
                                            width=self.TAPS_WIDTH, height=consts.TAPS_HEIGHT,
                                            label="Lights", label_font=consts.FONT, label_color=0xff7e00,
                                            fill_color=0x5c5b5c, outline_color=0x767676,
                                            selected_fill=0x1a1a1a, selected_outline=0x2e2e2e,
                                            selected_label=0x525252)
        self.buttons.append(self.views['button_view1'])  # adding this button to the buttons group

        self.views['button_view2'] = Button(x=self.TAPS_WIDTH, y=0,
                                            width=self.TAPS_WIDTH, height=consts.TAPS_HEIGHT, name="music",
                                            label="Music", label_font=consts.FONT, label_color=0xff7e00,
                                            fill_color=0x5c5b5c, outline_color=0x767676,
                                            selected_fill=0x1a1a1a, selected_outline=0x2e2e2e,
                                            selected_label=0x525252)
        self.buttons.append(self.views['button_view2'])  # adding this button to the buttons group


        for b in self.buttons:
            self.displays.appendDisplay('splash',b.group)

    def handle_foo(self, sender, earg):
        touch = earg.touch_point
        for i, b in enumerate(self.buttons):
            if touch is not None and b.contains(touch) and b.name is not None:
                if b.name == 'lights':
                    self.displays.switchDisplay("view1")
                    #self.selectTab('view1')
                    self.lightbuttons.setVisible(True)
                    self.musicbuttons.setVisible(False)
                    #self.displays.popDisplay(2)
                    gc.collect()
                if b.name == "music":
                        self.displays.switchDisplay("view2")
                        #self.selectTab('view2')
                        #self.displays.popDisplay(1)
                        self.lightbuttons.setVisible(False)
                        self.musicbuttons.setVisible(True)
                        gc.collect()
                while earg.touch_point:
                    pass

    #def selectTab(self, view):
    #    for k, v in self.views.items():
    #        self.views[k].selected = False
    #
    #    self.views['button_'+view].selected=True
