import gc
from adafruit_pyportal import PyPortal
gc.collect()
import board
gc.collect()
import adafruit_touchscreen
gc.collect()
import consts
gc.collect()
import json
from secrets import secrets

class Screens:
    pyportal = None
    configlist = None
    def __init__(self):
        self.pyportal = PyPortal(debug=True, url=secrets['listingurl'])
        self.configlist = json.loads(self.pyportal.fetch())
        display = board.DISPLAY
        display.rotation = 0


    def getPortal(self):
        return self.pyportal

    def getConfig(self):
        return self.configlist
    # Backlight function
    # Value between 0 and 1 where 0 is OFF, 0.5 is 50% and 1 is 100% brightness.
    def set_backlight(self, val):
        val = max(0, min(1.0, val))
        board.DISPLAY.auto_brightness = False
        board.DISPLAY.brightness = val

    def setupTouchScreen(self):
        screen_width = consts.SCREEN_WIDTH
        screen_height = consts.SCREEN_HEIGHT
        ts = adafruit_touchscreen.Touchscreen(board.TOUCH_XL, board.TOUCH_XR,
                                            board.TOUCH_YD, board.TOUCH_YU,
                                            calibration=((5200, 59000), (5800, 57000)),
                                            size=(screen_width, screen_height))
        return ts
