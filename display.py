import gc
import displayio
gc.collect()
import time
gc.collect()
import board
gc.collect()

class Displays:
    displayslist = {}

    def __init__(self, num):
        for x in range(num):
            if x == 0:
                self.displayslist['splash'] = displayio.Group(max_size=6)
            
            else:
                self.displayslist['view'+str(x)] = displayio.Group(max_size=15)

    def hideLayer(self,hide_target):
        try:
            self.displayslist['splash'].remove(self.displayslist[hide_target])
        except ValueError:
            pass

    def showLayer(self,show_target):
        try:
            time.sleep(0.1)
            self.displayslist['splash'].append(self.displayslist[show_target])
        except ValueError:
            pass

    def appendDisplay(self,name, obj):
        self.displayslist[name].append(obj)

    def clearDisplay(self, name):
        z = len(self.displayslist[name])
        print("range is: "+str(z))
        for v in range(z):
            print("clearing: "+str(v))
            self.displayslist[name].pop(0)


    def addDisplay(self, name, size=15):
        self.displayslist[name] = displayio.Group(max_size=15)

    def showDisplay(self,name):
        d = self.displayslist[name]
        board.DISPLAY.show(d)


    def switchDisplay(self, name):
        for key in self.displayslist:
            self.hideLayer(key)

        self.showLayer(name)

    def destroyDisplay(self, name):
        pass

    def popDisplay(self, name):
        #i = self.displays['splash'].index(name)
        del self.displayslist[name]
        #self.displayslist['splash'].pop(idx)
