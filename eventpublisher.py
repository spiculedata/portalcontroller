#!/usr/bin/env python
"""Sample script which uses event.py (simple). """

from source import event


class Publisher(object):

    def __init__(self):
        # Set event object
        self.evt_foo = event.Event()
        self.evt_timedtrigger = event.Event()


    def foo(self, arg):
            # Call event object with self as a sender
            self.evt_foo(self, arg)

    def timedtrigger(self, arg):
        # Call event object with self as a sender
        self.evt_timedtrigger(self, arg)
