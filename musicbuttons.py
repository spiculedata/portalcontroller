import gc
from adafruit_button import Button
gc.collect()
import consts
gc.collect()
import adafruit_requests as requests
gc.collect()
from secrets import secrets
gc.collect()
gc.collect()

class MusicButtons:
    buttons = []

    BUTTON_WIDTH = 100
    BUTTON_HEIGHT = 100
    SMALL_BUTTON_WIDTH = 60
    SMALL_BUTTON_HEIGHT = 50

    musicloaded = False
    controlsloaded = False
    viewlisting = False
    chunk = 0

    visible  = False

    shuffle = False
    repeat = False
    attempts = 3  # Number of attempts to retry each request
    failure_count = 0
    response = None
    failure_count2 = 0
    response2 = None

    def __init__(self, displays, viewname, publisher, config):
        self.displays = displays
        publisher.evt_foo += self.handle_foo
        self.restore()
        self.musiclist = config



    def restore(self):
        self.fetch_state()
        self.clearView()
        self.buttons = []
        button_icon = Button(x=0, y=60, name="controls",
                             width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT,
                             label="Controls", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)
        self.buttons.append(button_icon)
        self.displays.appendDisplay('view2', button_icon.group)

        button_icon = Button(x=105, y=60, name="play",
                             width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT,
                             label="Play", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)
        self.buttons.append(button_icon)

        self.displays.appendDisplay('view2', button_icon.group)

        button_icon = Button(x=210, y=60, name="music",
                             width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT,
                             label="Music", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)
        self.buttons.append(button_icon)

        self.displays.appendDisplay('view2', button_icon.group)

    def fetch_state(self):
        try:
            resp = requests.get('http://192.168.8.108:5005/'+secrets['playerid']+'/state')
            print(resp.json())
            if resp.json()['playMode']['repeat'] == "none":
                self.repeat = False
            else:
                self.repeat = True

            self.shuffle = resp.json()['playMode']['shuffle']
        except Exception as e:
            pass

    def load_control_buttons(self):
        if not self.controlsloaded:
            button4 = Button(x=0, y=60, name="repeat",
                             width=self.SMALL_BUTTON_WIDTH, height=self.SMALL_BUTTON_HEIGHT,
                             label="Repeat", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)

            self.displays.appendDisplay('view2', button4.group)
            self.buttons.append(button4)

            button5 = Button(x=80, y=60, name="play",
                             width=self.SMALL_BUTTON_WIDTH, height=self.SMALL_BUTTON_HEIGHT,
                             label="Play", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)

            self.displays.appendDisplay('view2', button5.group)
            self.buttons.append(button5)

            button6 = Button(x=170, y=60, name="shuffle",
                             width=self.SMALL_BUTTON_WIDTH, height=self.SMALL_BUTTON_HEIGHT,
                             label="Shuffle", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)

            self.displays.appendDisplay('view2', button6.group)
            self.buttons.append(button6)

            button7 = Button(x=0, y=130, name="prev",
                             width=self.SMALL_BUTTON_WIDTH, height=self.SMALL_BUTTON_HEIGHT,
                             label="BACK", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)

            self.displays.appendDisplay('view2', button7.group)
            self.buttons.append(button7)
            #
            button8 = Button(x=80, y=130, name="forward",
                             width=self.SMALL_BUTTON_WIDTH, height=self.SMALL_BUTTON_HEIGHT,
                             label="FWD", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)

            self.displays.appendDisplay('view2', button8.group)
            self.buttons.append(button8)



    def load_music_list(self):
        if not self.musicloaded:
            ypos = 60
            start = self.chunk
            end = self.chunk+3
            print("fetching: "+str(start)+" end:"+str(end))
            for value in self.musiclist[start:end]:
                print("Creating button for: "+[value][0]['uri'])
                button = Button(x=0, y=ypos,
                                width=150, height=40,
                                style=Button.SHADOWROUNDRECT,
                                fill_color=0x8900ff, outline_color=0x222222,
                                name=[value][0]['uri'], label_font=consts.FONT, label_color=0xffffff,
                                label=[value][0]['label'])
                ypos = ypos+45
                gc.collect()
                self.displays.appendDisplay('view2', button.group)
                gc.collect()
                self.buttons.append(button)
                gc.collect()

            button = Button(x=0, y=ypos,
                            width=60, height=40,
                            style=Button.SHADOWROUNDRECT,
                            fill_color=0x8900ff, outline_color=0x222222,
                            name='N', label_font=consts.FONT, label_color=0xffffff,
                            label='N')
            gc.collect()

            self.displays.appendDisplay('view2', button.group)
            gc.collect()

            self.buttons.append(button)
            gc.collect()

            self.chunk = self.chunk+1
            gc.collect()
            self.musicloaded = True


    def handle_foo(self, sender, earg):
        if self.visible:
            touch = earg.touch_point
            print(touch)
            for i, b in enumerate(self.buttons):
                if b is not None and touch is not None and b.contains(touch):
                        if b.name == 'controls' and self.viewlisting ==False:
                            print('controls pushed')
                            self.clearView()
                            self.buttons=[]
                            self.load_control_buttons()
                            while earg.touch_point:
                                pass
                        elif b.name == 'play' and self.viewlisting ==False:
                            self.playpause()
                            while earg.touch_point:
                                pass
                        elif b.name == 'music' and self.viewlisting ==False:
                            print('music pushed')
                            self.chunk = 0
                            self.viewlisting = True
                            self.clearView()
                            self.buttons=[]
                            self.load_music_list()
                            while earg.touch_point:
                                pass
                        elif b.name.startswith('spotify'):
                            print('album pushed')
                            print(b.name)
                            self.playAlbum(b.name)
                            while earg.touch_point:
                                pass
                        elif b.name == 'N':
                            print("N Pressed! :" +b.name + " "+b.label)
                            while earg.touch_point:
                                pass
                            self.musicloaded=False
                            self.clearView()
                            self.buttons=[]
                            self.load_music_list()
                            break
                        elif b.name == 'repeat':
                            self.repeattrack()
                        elif b.name == 'shuffle':
                            self.shuffleplaylist()
                        elif b.name == 'prev':
                            self.rew()
                        elif b.name == 'forward':
                            self.skip()

    def playpause(self):
        self.sonosRequest('/playpause')

    def skip(self):
        self.sonosRequest('/next')

    def rew(self):
        self.sonosRequest('/previous')

    def shuffleplaylist(self):
        if self.shuffle:
            self.sonosRequest('/shuffle/off')
            self.shuffle = False
        else:
            self.sonosRequest('/shuffle/on')
            self.shuffle = True


    def repeattrack(self):
        if self.repeat:
            self.sonosRequest('/repeat/off')
            self.repeat = False
        else:
            self.sonosRequest('/repeat/on')
            self.repeat = True

    def clearView(self):
        self.displays.clearDisplay("view2")

    def setVisible(self, vis):
        self.visible = vis
        self.viewlisting = False
        self.musicloaded = False
        self.restore()

    def sonosRequest(self, url):
        self.response = None
        while not self.response:
            try:
                print('requesting: '+url)
                self.response = requests.get('http://192.168.8.108:5005/'+secrets['playerid']+url)
                failure_count = 0
            except RuntimeError as error:
                pass
            except AssertionError as error:
                print("Request failed, retrying...\n", error)
                failure_count += 1
                if failure_count >= self.attempts:
                    raise AssertionError("Failed to resolve hostname, \
                                                  please check your router's DNS configuration.")
                continue

        if self.response:
            self.response.close()


    def playAlbum(self, uri):
        print("requesting album")
        self.response2 = None
        while not self.response2:
            try:
                print('requesting: '+uri)
                self.response2 = requests.get('http://192.168.8.108:5005/'+secrets['playerid']+'/spotify/clearqueueandplayalbum/'+uri)
                failure_count2 = 0
            except AssertionError as error:
                print("Request failed, retrying...\n", error)
                failure_count2 += 1
                if failure_count2 >= self.attempts:
                    raise AssertionError("Failed to resolve hostname, \
                                                  please check your router's DNS configuration.")
                continue

        if self.response2:
            self.response2.close()
