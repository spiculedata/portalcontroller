import gc
from adafruit_button import Button
gc.collect()
import consts
gc.collect()
import adafruit_requests as requests
gc.collect()
from secrets import secrets
gc.collect()
from random import randint
gc.collect()
import busio
gc.collect()
import adafruit_adt7410
gc.collect()
import board
gc.collect()

class LightButtons:

    temp_sensor = adafruit_adt7410.ADT7410(
        busio.I2C(board.SCL, board.SDA),
        address=0x48,  # Specific device address for ADT7410
    )
    temp_sensor.high_resolution = True

    visible = False
    pmode = False
    tmode = False
    RED = (255, 0, 0)
    ORANGE = (255, 34, 0)
    YELLOW = (255, 170, 0)
    GREEN = (0, 255, 0)
    CYAN = (0, 255, 255)
    BLUE = (0, 0, 255)
    VIOLET = (153, 0, 255)
    MAGENTA = (255, 0, 51)
    PINK = (255, 51, 119)
    AQUA = (85, 125, 255)
    WHITE = (255, 255, 255)
    OFF = (0, 0, 0)
    
    spots = [
        {'label': "1", 'pos': (10, 60), 'size': (50, 50), 'color': RED, 'hsv': '0, 100, 100'},
        {'label': "2", 'pos': (90, 60), 'size': (50, 50), 'color': ORANGE, 'hsv': '36, 100, 100'},
        {'label': "3", 'pos': (170, 60), 'size': (50, 50), 'color': YELLOW, 'hsv': '57, 100, 100'},
        {'label': "4", 'pos': (250, 60), 'size': (50, 50), 'color': GREEN, 'hsv': '120, 100, 100'},
        {'label': "5", 'pos': (10, 120), 'size': (50, 50), 'color': CYAN, 'hsv': '176, 100, 100'},
        {'label': "6", 'pos': (90, 120), 'size': (50, 50), 'color': BLUE, 'hsv': '241, 100, 100'},
        {'label': "7", 'pos': (170, 120), 'size': (50, 50), 'color': VIOLET, 'hsv': '285, 100, 100'},
        {'label': "8", 'pos': (250, 120), 'size': (50, 50), 'color': MAGENTA, 'hsv': '293, 100, 100'},
        {'label': "9", 'pos': (10, 180), 'size': (50, 50), 'color': PINK, 'hsv': '304, 100, 100'},
        {'label': "10", 'pos': (90, 180), 'size': (50, 50), 'color': AQUA, 'hsv': '198, 100, 100'},
        {'label': "11", 'pos': (170, 180), 'size': (50, 50), 'color': WHITE, 'hsv': '0, 0, 100'},
        {'label': "12", 'pos': (250, 180), 'size': (50, 50), 'color': OFF, 'hsv': '0, 0, 0'}
        ]
    
    buttons = []

    BUTTON_WIDTH = 100
    BUTTON_HEIGHT = 100
    lights_loaded = False

    attempts = 3  # Number of attempts to retry each request
    failure_count = 0
    response = None

    def calculated_light_hsv(self):
        degrees_celsius = self.temp_sensor.temperature
        print('Temperature: %s C' % degrees_celsius)
        max = 40
        min = 15

        #v = (max-min)/degrees_celsius
        v = ((degrees_celsius - min) * 100) / (max - min)
        print("V:"+str(v))
        x= 250-((v/100)*250)
        print("X: "+str(x))
        return x





    def handle_foo(self, sender, earg):
        degrees_celsius = self.temp_sensor.temperature
        if self.visible:
            touch = earg.touch_point
            for i, b in enumerate(self.buttons):
                if b is not None and touch is not None and b.contains(touch):
                    if b is not None:
                        z = None
                        if b.name is not None:
                            if b.name == 'mainlight':
                                self.clearView()
                                self.buttons=[]
                                self.load_lightbuttons()
                            try:
                                z = int(b.name)
                            except ValueError as verr:
                                pass
                            except Exception as ex:
                                pass
                            if z is not None:
                                spot = self.spots[z-1]
                                color = spot['hsv']
                                if z == 10:
                                    self.pmode = True
                                    self.tmode = False
                                elif z == 9:
                                    self.tmode = True
                                    self.pmode = False
                                else:
                                    self.pmode = False
                                    self.tmode = False
                                    self.changeLight(color)

                    while earg.touch_point:
                        pass

    def looper(self, sender, earg):
        h=0
        if self.pmode:
            print("pmode")
            h = randint(0, 360)
        if self.tmode:
            print("tmode")
            h = self.calculated_light_hsv()

        if self.pmode or self.tmode:
            self.response = None
            failure_count = 0
            while not self.response:
                try:
                    print("HSV:" + str(h)+', '+str(100)+', '+str(100))
                    self.response = requests.post('http://192.168.8.108:8080/rest/items/'+secrets['mainlightid'], data=str(h)+', '+str(100)+', '+str(100), headers={"Accept":"application/json", "Content-Type": "text/plain"})
                    failure_count = 0
                except AssertionError as error:
                    print("Request failed, retrying...\n", error)
                    failure_count += 1
                    if failure_count >= self.attempts:
                        raise AssertionError("Failed to resolve hostname, \
                                                      please check your router's DNS configuration.")
                    continue

                except RuntimeError as error:
                    print("Request failed to read....\n", error)
                    failure_count += 1
                    if failure_count >= self.attempts:
                            raise AssertionError("Failed to resolve hostname, \
                                                          please check your router's DNS configuration.")
                    continue
                if self.response:
                    self.response.close()


    def changeLight(self, hsv):
        failure_count = 0
        self.response = None
        while not self.response:
            try:
                self.response = requests.post('http://192.168.8.108:8080/rest/items/'+secrets['mainlightid'], data=hsv, headers={"Accept":"application/json", "Content-Type": "text/plain"})
                failure_count = 0
            except AssertionError as error:
                print("Request failed, retrying...\n", error)
                failure_count += 1
                if failure_count >= self.attempts:
                    raise AssertionError("Failed to resolve hostname, \
                                              please check your router's DNS configuration.")
                continue

        if self.response:
            self.response.close()

    def __init__(self, displays, viewname, publisher):
        self.displays = displays
        #self.load_lightbuttons()
        publisher.evt_foo += self.handle_foo
        publisher.evt_timedtrigger += self.looper
        self.restore()

    def clearView(self):
        self.displays.clearDisplay("view1")

    def load_lightbuttons(self):
        for spot in self.spots:
           button = Button(x=spot['pos'][0], y=spot['pos'][1],
                           width=spot['size'][0], height=spot['size'][1],
                           style=Button.SHADOWROUNDRECT,
                           fill_color=spot['color'], outline_color=0x222222, label_font=consts.FONT,
                           name=spot['label'])
           if spot['label']=="9":
                button.label="T"
           if spot['label']=="10":
               button.label="P"

           self.displays.appendDisplay('view1', button.group)
           self.buttons.append(button)
           self.lights_loaded = True

    def restore(self):
        self.clearView()
        self.buttons = []
        button_icon = Button(x=110, y=60, name="mainlight",
                             width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT,
                             label="Main Light", label_font=consts.FONT, label_color=0xffffff,
                             fill_color=0x8900ff, outline_color=0xbc55fd,
                             selected_fill=0x5a5a5a, selected_outline=0xff6600,
                             selected_label=0x525252, style=Button.ROUNDRECT)
        self.buttons.append(button_icon)
        self.displays.appendDisplay('view1', button_icon.group)

    def setVisible(self, vis):
        self.visible = vis
        self.restore()
